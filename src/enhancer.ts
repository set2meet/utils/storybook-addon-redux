import * as types from './lib/actionTypes';
import { ActionCreator, AnyAction, Dispatch, Reducer, StoreEnhancer } from 'redux';
import { TStateChangeListener, IStoreExt } from './index';

export const mergeStateAction: ActionCreator<AnyAction> = (state: any) => ({
  type: types.MERGE_STATE_TYPE,
  state,
});

export const setStateAction: ActionCreator<AnyAction> = (state: any) => ({
  type: types.SET_STATE_TYPE,
  state,
});

export const initialStateAction: ActionCreator<AnyAction> = () => ({
  type: types.INITIAL_STATE_TYPE,
});

export const enhanceReducer = (mainReducer: Reducer) => (state: any, action: any) => {
  switch (action.type) {
    case types.MERGE_STATE_TYPE:
      return { ...state, ...action.state };
    case types.SET_STATE_TYPE:
      return { ...action.state };
    default:
      return mainReducer(state, action);
  }
};

export const withReduxEnhancer: StoreEnhancer<IStoreExt> = (createStore) => (reducer, state) => {
  const store = createStore(enhanceReducer(reducer), state);
  let listener: TStateChangeListener = null;
  let actions: any[] = [];

  const enhanceDispatch = (dispatch: Dispatch) => (action: any) => {
    const prev = store.getState();
    const result = dispatch(action);
    const next = store.getState();

    actions.push(action);
    if (listener) listener(action, prev, next);

    return result;
  };

  return {
    ...store,
    dispatch: enhanceDispatch(store.dispatch),
    __WITH_REDUX_ENABLED__: {
      listenToStateChange: (l) => (listener = l),
    },
    getActions() {
      return actions;
    },
    clearActions() {
      actions = [];
    },
  };
};
