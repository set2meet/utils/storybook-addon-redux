import React, { ComponentType, ReactNode } from 'react';
import addons, { makeDecorator, OptionsParameter, StoryContext, StoryGetter, WrapperSettings } from '@storybook/addons';
import { diff as differ } from 'jsondiffpatch';
import { v4 as uuidv4 } from 'uuid';
import { ProviderProps } from 'react-redux';
import * as events from './lib/events';
import { connectCallbacks } from './registerStore';
import { AnyAction, Dispatch, Store } from 'redux';

export interface IWithReduxAction {
  name: string;
  action: AnyAction;
  dispatch?: () => void;
}

export type TActionsFactory = () => IWithReduxAction[];

export interface IWithReduxOptions extends OptionsParameter {
  Provider: ComponentType<ProviderProps>;
  store: Store & IStoreExt;
}

export interface IWithReduxWrapperSettings extends WrapperSettings {
  options: IWithReduxOptions;
  parameters: {
    actionsFactory?: TActionsFactory;
  };
}

export type TStateChangeListener = (action: AnyAction, prev: object, next: object) => void;

export interface IStoreExt {
  __WITH_REDUX_ENABLED__: {
    listenToStateChange: (l: TStateChangeListener) => void;
  };
  dispatch: Dispatch;
  getActions: () => AnyAction[];
  clearActions: () => void;
}

export interface IReduxWrapperProps extends IWithReduxWrapperSettings {
  children: ReactNode;
}

export interface IReduxWrapperState {
  disconnectCallbacks: () => void;
}

export class ReduxWrapper extends React.Component<IReduxWrapperProps, IReduxWrapperState> {
  public componentDidMount() {
    const { options, parameters } = this.props;
    const actionsFactory: TActionsFactory = parameters?.actionsFactory || (() => []);
    const { store } = options;
    const channel = addons.getChannel();

    channel.emit(events.INIT, {
      state: store.getState(),
      actions: actionsFactory(),
    });

    const onDispatchListener: TStateChangeListener = (action: AnyAction, prev: object, next: object) => {
      let diff;

      try {
        diff = differ(prev, next);
      } catch (e) {}

      const date = new Date();

      channel.emit(events.ON_DISPATCH, {
        id: uuidv4(),
        date,
        action,
        diff,
        prev,
        next,
      });
    };

    store.__WITH_REDUX_ENABLED__.listenToStateChange(onDispatchListener);

    const disconnectCallbacks = connectCallbacks(store);

    this.setState({
      disconnectCallbacks,
    });
  }

  public componentWillUnmount() {
    this.state.disconnectCallbacks();
  }

  public render() {
    const { options, children } = this.props;

    if (!options) throw new Error('withRedux: options are required');

    const { Provider, store } = options;

    if (!store) throw new Error('withRedux: store is required');
    if (!store.__WITH_REDUX_ENABLED__) throw new Error('withRedux enhancer is not enabled in the store');

    return <Provider store={store}>{children}</Provider>;
  }
}

export const withReduxDecoratorProperties = {
  name: 'withRedux',
  parameterName: 'redux',
  skipIfNoParametersOrOptions: false,
  allowDeprecatedUsage: true,
  wrapper: (getStory: StoryGetter, context: StoryContext, settings: IWithReduxWrapperSettings) => {
    return <ReduxWrapper {...settings}>{getStory(context)}</ReduxWrapper>;
  },
};

export const withRedux = makeDecorator({
  ...withReduxDecoratorProperties,
});

export * from './enhancer';

if (module && module.hot && module.hot.decline) {
  module.hot.decline();
}
