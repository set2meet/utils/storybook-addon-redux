import addons from '@storybook/addons';
import { STORY_CHANGED } from '@storybook/core-events';
import * as events from './lib/events';
import { initialStateAction, setStateAction } from './enhancer';

function setInitialState() {
  const channel = addons.getChannel();

  channel.emit(events.DISPATCH, initialStateAction());
}

const setState = (store) => (state) => store.dispatch(setStateAction(state));
const dispatchAction = (store) => (action) => {
  store.dispatch(action);
};

const disconnectCallbacks = (setStateListener, dispatchActionListener) => () => {
  const channel = addons.getChannel();

  channel.removeListener(events.SET_STATE, setStateListener);
  channel.removeListener(events.DISPATCH, dispatchActionListener);

  channel.removeListener(STORY_CHANGED, setInitialState);
  channel.removeListener(events.RESET, setInitialState);
};

export const connectCallbacks = (store) => {
  const channel = addons.getChannel();
  const setStateListener = setState(store);
  const dispatchActionListener = dispatchAction(store);

  channel.on(events.SET_STATE, setStateListener);
  channel.on(events.DISPATCH, dispatchActionListener);

  channel.on(STORY_CHANGED, setInitialState);
  channel.on(events.RESET, setInitialState);

  return disconnectCallbacks(setStateListener, dispatchActionListener);
};
