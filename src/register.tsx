import React from 'react'
import addons from '@storybook/addons'
import HistoryPanel from './lib/components/HistoryPanel'
import StatePanel from './lib/components/StatePanel'
import './lib/style'

addons.register('storybook/with_redux', api => {
  addons.addPanel('storybook/with_redux/state', {
    title: 'Redux State',
    render: ({ active, key } = { active: false, key: '' }) => <StatePanel api={api} key='Redux State Panel' active={active} />
  })

  addons.addPanel('storybook/with_redux/history', {
    title: 'Redux History',
    render: ({ active, key } = { active: false, key: '' }) => <HistoryPanel api={api} key='Redux History Panel' active={active} />
  })
})
