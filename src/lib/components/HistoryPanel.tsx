import React from 'react'
import { STORY_CHANGED } from '@storybook/core-events';
import * as events from '../events'
import StateChange from './StateChange'
import { setStateAction } from '../../enhancer'
import * as types from '../actionTypes'
import {AnyAction} from "redux";
import {API} from "@storybook/api";

export interface Change {
    id: number,
    date: (string | Date),
    action: AnyAction,
    diff?: object,
    prev: object,
    next: object,
    dispatchSetState(): void,
}

export interface IHistoryPanelProps {
    active: boolean,
    api: API,
}

export interface IHistoryPanelState {
    changes: Change[],
    enabled: boolean,
    actionFilter: string,
    diffFilter: string,
}

class HistoryPanel extends React.Component<IHistoryPanelProps, IHistoryPanelState> {
    public state: IHistoryPanelState = {
        changes: [],
        enabled: true,
        actionFilter: '',
        diffFilter: '',
    }
    private stopListeningOnStory: any;

    constructor(props: IHistoryPanelProps) {
        super(props);
    }

    setChanges = (changes: Change[]) => this.setState({ changes });
    setEnabled = (enabled: boolean) => this.setState({ enabled });
    setActionFilter = (actionFilter: string) => this.setState({actionFilter});
    setDiffFilter = (diffFilter: string) => this.setState({diffFilter});

    onInit = () => () => {
        this.setChanges([])
        this.setEnabled(true)
    }
    onDispatch = (change?: Change) => {
        if (!change) {
            this.setEnabled(false)
            this.setChanges([])
        } else if (!isWithReduxChange(change)) {
            change = {
                ...change,
                dispatchSetState: () => this.props.api.emit(events.DISPATCH, setStateAction(change.next))
            }
            this.setChanges([change, ...this.state.changes.slice(0, 100)])
            this.setEnabled(true)
        }
    }
    onActionFilter = (ev: React.ChangeEvent<HTMLInputElement>) => this.setActionFilter(ev.target.value)
    onDiffFilter = (ev: React.ChangeEvent<HTMLInputElement>) => this.setDiffFilter(ev.target.value)

    changeIsVisible = (change: Change) => {
        const {actionFilter, diffFilter} = this.state;
        if (!actionFilter && !diffFilter) return true
        if (actionFilter && matches(actionFilter, change.action)) return true
        if (diffFilter && matches(diffFilter, change.diff)) return true
        return false
    }

    reset = () => {
        this.props.api.emit(events.RESET)
        this.setChanges([])
    }

    componentDidMount() {
        const {api} = this.props
        api.on(events.INIT, this.onInit)
        api.on(events.ON_DISPATCH, this.onDispatch)
        this.stopListeningOnStory = api.on(STORY_CHANGED, () => {
            this.onDispatch()
        })
    }

    componentWillUnmount() {
        const {api} = this.props
        if (this.stopListeningOnStory) {
            this.stopListeningOnStory()
        }
        api.off(events.INIT, this.onInit)
        api.off(events.ON_DISPATCH, this.onDispatch)
    }

    public render() {
        const {active} = this.props;
        const {changes, enabled, actionFilter, diffFilter} = this.state;
        if (!active) return null
        if (!enabled) return <div className='addon-redux-disabled'>withRedux Not Enabled</div>

        return (
            <table className='addon-redux addon-redux-history-panel'>
                <thead>
                <tr>
                    <th>Time</th>
                    <th><input value={actionFilter} onChange={this.onActionFilter} placeholder='ACTION (filter)'/></th>
                    <th><input value={diffFilter} onChange={this.onDiffFilter} placeholder='STATE DIFF (filter)'/></th>
                    <th>Previous State</th>
                    <th>Next State</th>
                    <th>
                        <button onClick={this.reset}>RESET</button>
                    </th>
                </tr>
                </thead>
                <tbody>
                {changes.filter(this.changeIsVisible).map(mapSateChange)}
                </tbody>
            </table>
        )
    }
}

const mapSateChange = (change: Change, index: number) => <StateChange {...change} key={change.id}/>

const isWithReduxChange = (change: Change) => change.action && Object.values(types).includes(change.action.type)

const matches = (filter: any, obj: any) => {
    const re = new RegExp('.*?' + filter + '.*', 'i')
    return JSON.stringify(obj).match(re) !== null
}

export default HistoryPanel

