import React, { ChangeEvent, useState } from 'react';
import { API } from '@storybook/api';
import * as events from '../events';

export interface IEditModeProps {
  api: API;
  state: object;
  setViewMode(): void;
}

export const EditMode = ({ api, state, setViewMode }: IEditModeProps) => {
  const [json, setJson] = useState(JSON.stringify(state, null, 2));

  const onChange = (event: ChangeEvent<HTMLTextAreaElement>) => setJson(event.target.value);

  const saveJson = () => {
    api.emit(events.SET_STATE, JSON.parse(json));
    setViewMode();
  };

  return (
    <div>
      <textarea value={json} onChange={onChange} />
      <div>
        <button onClick={setViewMode}>Cancel</button>
        <button onClick={saveJson}>Save</button>
      </div>
    </div>
  );
};

export default EditMode;
