import React from 'react'
import { STORY_CHANGED } from '@storybook/core-events';
import { CustomizedJsonTree } from './Json'
import * as events from '../events'
import SetStateForm, {IEditModeProps} from './SetStateForm'
import {API} from "@storybook/api";
import {Change} from "./HistoryPanel";
import {IWithReduxAction} from "../../index";

export interface IHistoryPanelProps {
  active: boolean,
  api: API,
}

export enum Mode {
  VIEW = 'view',
  EDIT = 'edit',
}

export interface IStatePanelState {
  enabled: boolean,
  state: object,
  mode: Mode,
  actions: IWithReduxAction[],
}

class StatePanel extends React.Component<IHistoryPanelProps, IStatePanelState> {

  public state: IStatePanelState = {
    actions: [],
    enabled: false,
    mode: Mode.VIEW,
    state: {},
  }

  constructor(props: IHistoryPanelProps) {
    super(props);
  }

  private stopListeningOnStory: any;

  setNewState = (state: any) => this.setState({ state });
  setEnabled = (enabled: boolean) => this.setState({ enabled });
  setActions = (actions: IWithReduxAction[]) => this.setState({actions});
  setMode = (mode: Mode) => this.setState({mode});

  onInit = ({ state = {}, actions = [] }) => {
    this.setNewState(state)
    actions = actions.map(action => ({...action, dispatch: () => {
      this.props.api.emit(events.DISPATCH, action.action)
      }}))
    this.setActions(actions)
    this.setEnabled(true)
  }

  onDispatch = ({ action, diff, prev, next }: Change) => {
    this.setNewState(next)
  }

  setViewMode = () => this.setMode(Mode.VIEW)
  setEditMode = () => this.setMode(Mode.EDIT)

  componentDidMount () {
    const { api } = this.props
    api.on(events.INIT, this.onInit)
    api.on(events.ON_DISPATCH, this.onDispatch)
    this.stopListeningOnStory = this.props.api.on(STORY_CHANGED, () => {
      this.onInit({})
      this.setEnabled(false)
    })
  }

  componentWillUnmount () {
    const { api } = this.props
    api.off(events.INIT, this.onInit)
    api.off(events.ON_DISPATCH, this.onDispatch)
  }

  public render() {
    const props = this.props;
    const {mode, enabled} = this.state;
    if (!props.active) return null
    if (!enabled) return <div className='addon-redux-disabled'>withRedux Not Enabled</div>
    return (
        <div className='addon-redux'>
          {mode === Mode.EDIT ?
              <EditMode state={this.state.state} setViewMode={this.setViewMode} api={props.api} /> :
              <ViewMode state={this.state.state} setEditMode={this.setEditMode} actions={this.state.actions} />}
        </div>
    )
  }
}

const EditMode = (props: IEditModeProps) => <SetStateForm {...props} />

const ActionButton = ({ name, action, dispatch }: IWithReduxAction) =>
  <button key={name} onClick={dispatch} title={JSON.stringify(action, null, 2)}>{name}</button>

export interface IViewModeProps {
  state: object,
  setEditMode(): void,
  actions: IWithReduxAction[],
}

const ViewMode = ({ state, setEditMode, actions }: IViewModeProps) =>
  <div className='addon-redux-state-panel'>
    <div className='addon-redux-button-bar'>
      <button onClick={setEditMode}>Edit State</button>
      {actions.length ? actions.map(ActionButton) : <span className='addon-redux-no-actions'>NO CANNED ACTIONS</span>}
    </div>
    <div className='redux-addon-state-json'><CustomizedJsonTree data={state} /></div>
  </div>

export default StatePanel
