# Storybook Addon Redux

Storybook Redux Addon aids in using redux backed components in your stories in [Storybook](https://storybook.js.org).

Original package: [addon-redux](https://github.com/frodare/addon-redux).

Ideally stories are only needed for non-redux connected components, not containers.  However, when writing stories for components of a redux application, it is common for the components to have conatiners as children which causes problems.  This is where the Redux Addon helps out by providing a decorator and helpful panels to support container components.

---

![Redux Addon History Panel](docs/addon-redux-history-panel.png?v=1)

This addon is compatible with Storybook for React

# Features

- Add two panels to Storybook: Redux State and Redux History
- Wraps stories with a React Redux Provider component
- View and Edit the current state of the store
- Provides Canned Action buttons which can dispatch predefined actions
- Logs actions and maintains the time, action, previous state, next state and state diff
- Supports time travel to previous states
- Filter actions entries by action and state diff

# Usage

In order for the React Redux addon to function correctly:
- it must be [registered](#register) as a Storybook addon
- its store [enhancer](#enhancer) must be used in the provided store
- the [withRedux](#decorator-withredux) decorator must be used in the story

## Getting Started

First of all, you need to add Redux Addon into your project as a dev dependency.

```json
{
  "devDependencies": {
    "@s2m/storybook-addon-redux": "git+ssh://git@git.epam.com:epm-s2m/utils/storybook-addon-redux.git"
  }
}
```

within `.storybook/main.js`:

```js
module.exports = {
  addons: ['@s2m/storybook-addon-redux']
}
```

## Enhancer

To give the Redux Addon the ability to listen to and alter the store, its enhancer must be used when creating the store as shown below.

```js
// Simplest use of the Redux Addon Enhancer
import { createStore, compose } from 'redux'
import reducer from './your/reducer'
import withReduxEnhancer from '@s2m/storybook-addon-redux/enhancer'

const store = createStore(reducer, withReduxEnhancer)

export default store
```

Usually the store of an application will already be using enhancers. A more realistic store setup for the Redux Addon is shown below.
It includes the commonly used middleware enhancer along with some middlewares for demonstration.
This example shows how the Redux enhancer can be used with other enhancers, although the store creation code can look very different between different applications.

```js
// Realistic use of the Redux Addon Enhancer
import { createStore, compose } from 'redux'
import reducer from './your/reducer'
import createMiddlewareEnhancer from './middlewareEnhancer'
import { withReduxEnhancer } from '@s2m/storybook-addon-redux'
import { applyMiddleware } from 'redux'
import invariant from 'redux-immutable-state-invariant'
import logger from 'redux-logger'

createMiddlewareEnhancer = () => {
  const middleware = []
  if (process.env.NODE_ENV !== 'production') {
    // include other middlewares as needed, like the invariant and logger middlewares
    middleware.push(invariant())
    middleware.push(logger())
  }
  return applyMiddleware(...middleware)
}

const createEnhancer = () => {
  const enhancers = []
  enhancers.push(createMiddlewareEnhancer())
  if (process.env.NODE_ENV !== 'production') {
    enhancers.push(withReduxEnhancer)
  }
  return compose(...enhancers)
}

const store = createStore(reducer, createEnhancer())

export default store
```

## Decorator (withRedux)

The Redux Addon provides a decorator that wraps stories with a react redux provider component.
The result then needs to be invoked with the redux addon settings for the story.
There are currently following supported settings: 
__store__ and __actionsFactory__.  
You must also specify __Provider__.

```js
import React from 'react'
import { Provider } from 'react-redux'
import { withRedux } from '@s2m/storybook-addon-redux'
import configureStore from './your/configureStore'
import Container from './your/container'

const withReduxSettings = {
  Provider,
  store: configureStore(),
}

export default {
  title: 'Demo',
  component: Container,
  excludeStories: /.*Action$/,
};

export const Default = () => <Container />;

Default.story = {
  decorators: [withRedux(withReduxSettings)],
  parameters: {
    redux: {
      actionsFactory: () => [{name: 'Demo Action', action: {type: 'test'}}]
    }
  }
}
```

### Store Setting
To function properly, the store must be built including the [enhancer](#enhancer) provided with the Redux Addon.

### Canned Actions Setting
The __actionsFactory__ setting is optional. Use it to set canned (predefined) actions that are useful to test the story's component.
The setting takes a function which returns an array of canned action objects which contain a name and action key.
A button will be appended to the Redux State Panel for each canned action object in the array.
The name key is used as the label of a button.
The action key holds the action object that will be dispatched when the canned action button is clicked.

```js
// Canned Action Object
{
  name: 'Test Action',
  action: {
    type: 'test_action_type',
    payload: 'action data'
  }
}

```

![Redux Addon State Panel](docs/addon-redux-state-panel.png?v=1)
